﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateShoot : MonoBehaviour
{
    public List<GameObject> shootPlace;
    public GameObject prefabMissil;
    private bool shooting;
    void Start()
    {
        shooting = false;
    }
    void Update()
    {
        if (!shooting)
        {
            StartCoroutine(Cooldown());
        }
    }
    IEnumerator Cooldown()
    {
        shooting = true;
        foreach(GameObject sp in shootPlace)
        {
            GameObject bullet = Instantiate(prefabMissil, sp.transform.position, new Quaternion(0, 0, 180, 0));
            bullet.GetComponent<Rigidbody2D>().AddForce(Vector3.up * -3f, ForceMode2D.Impulse);
        }
        yield return new WaitForSeconds(2);
        shooting = false;
    }

}
