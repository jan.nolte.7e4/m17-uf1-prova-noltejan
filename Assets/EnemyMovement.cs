﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public int hp=1;
    public float velMult=1f;
    public int scoreEarn = 5;
    void Start()
    {        
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector3.down * velMult, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("BorderCollider"))
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        else if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }
    public void loseHP()
    {
        hp--;
        if (hp <= 0)
        {
            GameManager.Instance.addScore(scoreEarn);           
            Destroy(this.gameObject);
        }
    }
}
