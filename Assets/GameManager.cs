﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    private static int _score;
    public static int Score
    {
        get
        {
            return _score;
        }
    }
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        //DontDestroyOnLoad(this);
    }

    void Start()
    {
        _score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addScore(int? val)
    {
        if (player != null)
        {
            if (val.HasValue)
            {
                _score += val.Value;
            }
            else
            {
                _score += 1;
            }
        }     
        Debug.Log(Score);
    }

}
