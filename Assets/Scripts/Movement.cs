﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody2D playerRig;
    public float playerSpeed;    
    void Start()
    {
        playerRig = this.gameObject.GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
            Vector2 velocity = new Vector2();
            if (Input.GetKey(KeyCode.RightArrow))
            {
                velocity += Vector2.right;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                velocity += Vector2.left;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                velocity += Vector2.up;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                velocity += Vector2.down;
            }
            playerRig.velocity = velocity.normalized * ((playerSpeed>0)?playerSpeed:2) / 2;
        }
        else
        {
            playerRig.velocity = new Vector2(0, 0);
        }

    }
}
