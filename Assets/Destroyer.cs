﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("BorderCollider"))
        {
            if (collision.gameObject.CompareTag("PlayerWeapon"))
            {
                Destroy(collision.gameObject.GetComponentInParent<GameObject>());
            }else if (collision.gameObject.CompareTag("Player"))
            {
                Destroy(collision.gameObject);
            }
            else if (collision.gameObject.CompareTag("Enemy"))
            {
                collision.gameObject.GetComponent<EnemyMovement>().loseHP();
            }            
        }
        Destroy(this.gameObject);       
    }
}
