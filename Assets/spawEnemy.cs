﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawEnemy : MonoBehaviour
{
    public List<GameObject> enemies;
    // Start is called before the first frame update
    void Start()
    {
         InvokeRepeating("spawnEnemyShip", 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private void spawnEnemyShip()
    {
        Instantiate(Instantiate(enemies[Random.Range(0, enemies.ToArray().Length)], new Vector3(transform.position.x + Random.Range(-5, 5), transform.position.y, transform.position.z), Quaternion.identity));
    }
}
