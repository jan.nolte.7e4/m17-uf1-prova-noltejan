﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")){
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.CompareTag("BorderCollider"))
        {
            Destroy(this.gameObject);
        }
    }
}
