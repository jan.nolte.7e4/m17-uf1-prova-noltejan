﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponUse : MonoBehaviour
{
    public GameObject prefabBullet;
    public GameObject shootingPoint;
    void Start()
    {
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bullet = Instantiate(prefabBullet, shootingPoint.transform.position, new Quaternion(0, 0, 180, 0));
            bullet.GetComponent<Rigidbody2D>().AddForce(Vector3.up*3f, ForceMode2D.Impulse);
        }
    }
}
